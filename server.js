/*jshint node: true, esversion: 6 */
const express = require("express");
const app = express();
const port = process.env.port || 3000;
const path = require("path");

//https
const fs = require("fs");
// const https = require("https");
// const server = https.createServer({
//     key: fs.readFileSync("./ssl/my.key"),
//     cert: fs.readFileSync("./ssl/my.crt")
// }, app);
const server = require("http").createServer(app);

//db
const mongoose = require("mongoose");
mongoose.Promise = global.Promise;
const configDB = require("./config/database");
mongoose.connect(configDB.url);
const db = mongoose.connection;
db.on("open", () => {
    console.log("Połączono z MongoDB");
});
db.on("error", () => {
    console.error("Brak połączenia z MongoDB");
});

//register models
fs.readdirSync(path.join(__dirname, "models")).forEach((file) => {
    require(path.join(__dirname, "models", file));
});

//session, cookie, body-parser
//const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const session = require("express-session");
const mongoStore = require("connect-mongo")(session);
const mongoSessionStore = new mongoStore({ mongooseConnection: mongoose.connection });
const less = require("less-middleware");
const sessionKey = "mikrofb.sid";
const sessionSecret = "Ud3*xq%D";
// app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(session({
    key: sessionKey,
    secret: sessionSecret,
    resave: false,
    saveUninitialized: false,
    store: mongoSessionStore,
    cookie: {
        sameSite: true
    }
}));
app.use(less(path.join(__dirname, '/src'), {
    dest: path.join(__dirname, '/public')
}));
app.use(express.static(path.join(__dirname, 'public')));
app.set("view engine", "ejs");


//passport
const passport = require("passport");
const configPassport = require("./config/passport");
configPassport(passport);
app.use(passport.initialize());
app.use(passport.session());

//socket.io
const io = require("socket.io").listen(server);
const passportSocketIo = require("passport.socketio");
const onAuthorizeSuccess = (data, accept) => {
    accept();
};

const onAuthorizeFail = (data, message, error, accept) => {
    if (error) { // wystąpił błąd
        throw new Error(message);
    }
    // połączenie nieautoryzowane (ale nie błąd)
    console.log('Nieudane połączenie z socket.io');
    accept(new Error('Brak autoryzacji!'));
};

io.use(passportSocketIo.authorize({
  key:          sessionKey,
  secret:       sessionSecret,
  store:        mongoSessionStore,
  success:      onAuthorizeSuccess,
  fail:         onAuthorizeFail
}));

//routing
const routes = require("./routes/routes");
app.use(routes(passport, io, passportSocketIo));

// app.get("/", (req, res) => {
//     res.send("Hello!");
// });

server.listen(port, () => {
    console.log("Express listening on: https://localhost:" + port);
});
