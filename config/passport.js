/*jshint node:true*/
const mongoose = require("mongoose");
var LocalStrategy = require("passport-local").Strategy;
const User = mongoose.model("User");

module.exports = (passport) => {
    passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
    }, function (email, password, done) {
        User.findOne({ email: email }, (err, user) => {
            if (err) {
                done(err);
            }
            if (!user) {
                return done(null, false, { message: "Nieprawidłowy email" });
            }
            if (user.password === password) {
                done(null, user);
            } else {
                done(null, false, { message: "Nieprawidłowe hasło" });
            }
        });
    }));

    passport.serializeUser((user, done) => {
        done(null, user.id);
    });

    passport.deserializeUser((id, done) => {
        User.findById(id, (err, user) => {
            if (err) {
                done(err);
            }
            if (user) {
                done(null, {
                    id: user._id,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    email: user.email,
                    //friends: user.friends
                });
            }
        });
    });
};
