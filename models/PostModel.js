const mongoose = require("mongoose");

const PostSchema = new mongoose.Schema({
    content: {
        type: String,
        required: true
    },
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    userWall: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    dateCreated: {
        type: Date,
        default: Date.now
    },
    sharedBy: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
    }],
    likes: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
    }]
});

PostSchema.methods.toggleLike = function(userId) {
    var res = false;
    var self = this;
    this.likes.forEach(function(like) {
        if (like.toString() === userId.toString()) {
            res = true;
            self.likes.pull(like);
        }
    });

    if (!res) {
        this.likes.push(userId);
    }
    this.save();
    return !res;
};

PostSchema.methods.toggleShare = function(userId) {
    var res = false;
    var self = this;
    this.sharedBy.forEach(function(share) {
        if (share.toString() === userId.toString()) {
            res = true;
            self.sharedBy.pull(share);
        }
    });

    if (!res) {
        this.sharedBy.push(userId);
    }
    this.save();
    return !res;
};

module.exports = mongoose.model("Post", PostSchema);
