/* jshint node:true*/

const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    friends: [{
        friend: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
        },
        isFollowing: Boolean,
        unreadMessage: Boolean
    }],
    invitations: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
    }],
});

UserSchema.methods.isFriend = function(userFriend) {
    var ret = false;
    this.friends.forEach(function(element) {
        if (element.friend.toString() === userFriend.toString()) {
            ret = true;
        }
    });

    return ret;
};

UserSchema.methods.stopFollowing = function(friendId) {
    this.friends.forEach(function(element) {
        if (element.friend.toString() === friendId.toString()) {
            element.isFollowing = false;
        }
    });
    this.save(function(err) {
    });
};

UserSchema.methods.startFollowing = function(friendId) {
    this.friends.forEach(function(element) {
        if (element.friend.toString() === friendId.toString()) {
            element.isFollowing = true;
        }
    });
    this.save(function(err) {
    });
};

UserSchema.methods.getAllUsers = function (regexp, cb) {
    this.model("User").find({ $or: [{firstName: { $regex: regexp, $options: 'i' }}, {lastName: { $regex: regexp, $options: 'i' }}] }, "firstName lastName")
        .limit(5)
        .then((allUsers) => {
            var friendsIds = [];
            this.friends.forEach((element) => {
                friendsIds.push(element.friend.toString());
            });
            var ret = [];

            allUsers.forEach((element) => {
                var index = friendsIds.indexOf(element._id.toString());
                var obj = {_id: element._id, firstName: element.firstName, lastName: element.lastName};
                if (index > -1) {
                    obj.isFriend = true;
                    obj.isFollowing = this.friends[index].isFollowing;
                }
                ret.push(obj);
            });

            cb(ret);
        });
};
UserSchema.set('toJSON', {
    virtuals: true
});

UserSchema.set('toObject', {
    virtuals: true
});

UserSchema.virtual('fullName').get(function () {
    return this.firstName + " " + this.lastName;
});

UserSchema.statics.getFollowers = function(userId, cb) {
    this.find({$or: [{friends: {$elemMatch: {friend: userId, isFollowing: true}}}]})
        .then((followers) => {
            cb(followers);
        });
};

UserSchema.statics.isFollowing = function(userId, friendId, cb) {
    this.findById(userId)
        .then(user => {
            var res = false;
            user.friends.forEach(relObj => {
                if (relObj.friend.toString() === friendId.toString() && relObj.isFollowing) {
                    res = true;
                }
            });
            cb(res);
        });
};

module.exports = mongoose.model("User", UserSchema);
