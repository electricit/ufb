
module.exports = (authorize) => {
    var mongoose = require("mongoose");
    var User = mongoose.model("User");
    var authController = require("./authController");
    var router = require("express").Router();
    var views = require("../views/user/loader");

    router.post("/signup", (req, res) => {
        const regExp = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (!regExp.test(req.body.email)) {
            return res.json({ error: "Niepoprawny adres email" });
        }
        User
            .findOne({ email: req.body.email })
            .then(function (user) {
                if (user) {
                    return res.json({ error: "Podany email już jest zajęty" });
                }
                User.create(req.body);
                return res.json({ message: "account created" });
            });
    });

    router.get("/all", function(req, res) {
        var regExp = new RegExp(req.query.regex);
        User.findById(req.user.id)
            .then((user) => {
                var arr = user.getAllUsers(regExp, (arr) => {
                    res.json(arr);
                });
            });
    });

    return router;
};
