module.exports = (authorize, io, passportSocketIo) => {
    var mongoose = require("mongoose");
    var User = mongoose.model("User");
    var Post = mongoose.model("Post");
    var authController = require("./authController");
    var router = require("express").Router();
    var views = require("../views/post/loader");
    var path = require("path");
    var fs = require('fs');
    var file = fs.readFileSync(path.join(__dirname, "../views/post/index.ejs"), "utf-8");
    var postTemplate = require("ejs").compile(file);
    var postSocket = io.of("/post");
    var findSocket = function (socketId) {
        return postSocket.to("/post#" + socketId);
    };

    router.use(authorize);

    router.get("/get", (req, res) => {
        var userId = req.user.id;
        if (req.query.wallid === req.user.id.toString()) {
            User.findById(req.user.id)
                .then((user) => {
                    var wallsIds = [];
                    wallsIds.push(user._id);
                    user.friends.forEach(function (friend) {
                        if (friend.isFollowing) {
                            wallsIds.push(friend.friend);
                        }
                    });
                    Post.find({ $or: [{ userWall: { $in: wallsIds } }, { sharedBy: req.user.id }] })
                        .limit(5)
                        .sort({
                            dateCreated: "desc"
                        })
                        .populate("creator", {
                            _id: 1,
                            firstName: 1,
                            lastName: 1
                        })
                        .populate("sharedBy", {
                            _id: 1,
                            firstName: 1,
                            lastName: 1
                        })
                        .populate("likes", {
                            _id: 1,
                            firstName: 1,
                            lastName: 1
                        })
                        .populate("userWall", {
                            _id: 1,
                            firstName: 1,
                            lastName: 1
                        })
                        .then((posts) => {
                            var array = createPostsArray(posts, userId);
                            res.json({ posts: array });
                        });
                });
        } else {
            User.findById(req.params.wallid)
                .then((user) => {
                    userId = req.query.wallid;
                    Post.find({ $or: [{ userWall: req.query.wallid }, { sharedBy: req.query.wallid }] })
                        .limit(5)
                        .sort({
                            dateCreated: "desc"
                        })
                        .populate("creator", {
                            _id: 1,
                            firstName: 1,
                            lastName: 1
                        })
                        .populate("sharedBy", {
                            _id: 1,
                            firstName: 1,
                            lastName: 1
                        })
                        .populate("likes", {
                            _id: 1,
                            firstName: 1,
                            lastName: 1
                        })
                        .populate("userWall", {
                            _id: 1,
                            firstName: 1,
                            lastName: 1
                        })
                        .then((posts) => {
                            var array = createPostsArray(posts, userId);
                            res.json({ posts: array });
                        });
                });
        }
    });

    router.post("/load", (req, res) => {
        Post.find({ _id: { $in: req.body.array } })
            .populate("creator")
            .populate("userWall", {
                _id: 1,
                firstName: 1,
                lastName: 1
            })
            .then((posts) => {
                res.json({ posts: createPostsArray(posts, req.user) });
            });
    });

    router.post("/create", (req, res) => {
        var ObjectId = require('mongoose').Types.ObjectId;
        var user = User.findById(req.body.userwall)
            .then((user) => {
                if (!user) {
                    res.json({ error: "Nie ma takiego użytkownika" });
                }
                var post = new Post({
                    content: req.body.content,
                    creator: req.user.id,
                    userWall: req.body.userwall
                });
                post.save()
                    .then((err) => {
                        if (!err) {
                            res.json({ error: "Nie udało się dodać postu" });
                        }
                        Post.findById(post._id)
                            .populate("creator")
                            .exec((err, post) => {
                                res.json({ post: postTemplate({ post: post }) });
                            });
                    });

            });
    });

    postSocket.on("connection", (socket) => {
        socket.on("addPost", function (data) {
            //console.log("addPost")
            var user = User.findById(data.userwall)
                .then((user) => {
                    if ((!user || !user.isFriend(socket.request.user.id)) && user._id === socket.request.user.id) {
                        socket.emit("msg", "Nie można dodać postu");
                        return;
                    }
                    var post = new Post({
                        content: data.content,
                        creator: socket.request.user.id,
                        userWall: data.userwall
                    });
                    post.save((err, savedPost) => {
                        Post.findOne(savedPost)
                            .populate("creator", {
                                _id: 1,
                                firstName: 1,
                                lastName: 1
                            })
                            .populate("userWall", {
                                _id: 1,
                                firstName: 1,
                                lastName: 1
                            })
                            .then(populatedPost => {
                                var renderedPost = createPostsArray([populatedPost], socket.request.user.id)[0];
                                var responseJson = { renderedPost: renderedPost, post: populatedPost };

                                //send to followers
                                User.getFollowers(data.userwall, (followers) => {
                                    passportSocketIo.filterSocketsByUser(io, function (element) {
                                        var ret = false;
                                        followers.forEach(function (follower) {
                                            if (element.id.toString() === follower._id.toString() ||
                                                element.id.toString() === post.userWall.toString()) {
                                                ret = true;
                                            }
                                        });
                                        return ret;
                                    }).forEach(function (followerSocket) {
                                        findSocket(followerSocket.id).emit("followerPost", responseJson);
                                    });
                                });

                                //send to friends on wall
                                user.friends.forEach(function (friend) {
                                    var friendSocket = passportSocketIo.filterSocketsByUser(io, function (element) {
                                        return friend.friend.toString() === element.id.toString();
                                    })[0];
                                    if (friendSocket) {
                                        findSocket(friendSocket.id).emit("newPost", responseJson);
                                    }
                                });
                            });
                    });
                });
        });
        socket.on("loadPosts", function (data) {
            if (data.wallid === socket.request.user.id.toString()) {
                User.findById(socket.request.user.id)
                    .then((user) => {
                        var wallsIds = [];
                        wallsIds.push(user._id);
                        user.friends.forEach(function (friend) {
                            if (friend.isFollowing) {
                                wallsIds.push(friend.friend);
                            }
                        });
                        Post.find()
                            .and([
                                {$or: [{ userWall: { $in: wallsIds } }, { sharedBy: socket.request.user.id }]},
                                {_id: { $lt: data.lastId }}
                            ])
                            .sort({
                                dateCreated: "asc"
                            })
                            .limit(5)
                            .populate("creator", {
                                _id: 1,
                                firstName: 1,
                                lastName: 1
                            })
                            .populate("sharedBy", {
                                _id: 1,
                                firstName: 1,
                                lastName: 1
                            })
                            .populate("likes", {
                                _id: 1,
                                firstName: 1,
                                lastName: 1
                            })
                            .populate("userWall", {
                                _id: 1,
                                firstName: 1,
                                lastName: 1
                            })
                            .then((posts) => {
                                var array = createPostsArray(posts, socket.request.user.id);
                                socket.emit("nextPosts", { posts: array });
                            });
                    });
            } else {
                User.findById(data.wallid)
                    .then((user) => {
                        userId = data.wallid;
                        Post.find()
                            .and([
                                {$or: [{ userWall: data.wallid }, { sharedBy: data.wallid }]},
                                {_id: { $lt: data.lastId }}
                            ])
                            .limit(5)
                            .sort({
                                dateCreated: "desc"
                            })
                            .populate("creator", {
                                _id: 1,
                                firstName: 1,
                                lastName: 1
                            })
                            .populate("sharedBy", {
                                _id: 1,
                                firstName: 1,
                                lastName: 1
                            })
                            .populate("likes", {
                                _id: 1,
                                firstName: 1,
                                lastName: 1
                            })
                            .populate("userWall", {
                                _id: 1,
                                firstName: 1,
                                lastName: 1
                            })
                            .then((posts) => {
                                var array = createPostsArray(posts, userId);
                                socket.emit("nextPosts", { posts: array });
                            });
                    });
            }
        });
    });

    var createPostsArray = (posts, userId) => {
        var postArray = [];
        posts.forEach(function (element) {
            var user;
            element.sharedBy.forEach((shared) => {
                if (shared._id.toString() === userId.toString()) {
                    user = shared;
                }
            });
            postArray.push(postTemplate({ post: element, user: user }));
        });
        return postArray.reverse();
    };

    return router;
};
