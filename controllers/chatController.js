module.exports = (authorize, io, passportSocketIo) => {
    var router = require("express").Router();
    var chatSocket = io.of("/chat");
    var fs = require("fs");
    var util = require('util');
    var mongoose = require("mongoose");
    var User = mongoose.model("User");
    var Post = mongoose.model("Post");
    var Chat = mongoose.model("Chat");
    var Message = mongoose.model("Message");

    var findSocket = function (socketId) {
        return chatSocket.to("/chat#" + socketId);
    };
    chatSocket.on("connection", (socket) => {
        socket.on("msg", function (msg) {
            //console.log("msg")
            passportSocketIo.filterSocketsByUser(io, function (user) {
                console.log(user);
                return user === socket.request.user;
            }).forEach(function (element) {
                findSocket(element.id).emit("msg", msg);
            });
        });
        socket.on("openChat", function (friendId) {
            Chat.findOne({
                $or: [
                    { participants: [socket.request.user.id, friendId] },
                    { participants: [friendId, socket.request.user.id] }
                ]
            })
                .slice("history", -10)
                .populate({
                    path: "history",
                    limit: 15,
                    populate: {
                        path: "sender",
                        select: "firstName lastName"
                    }
                })
                .then((chat) => {
                    if (!chat) {
                        var newChat = new Chat({
                            participants: [socket.request.user.id, friendId],
                            history: []
                        }).save((err, res) => {
                            User.findById(friendId)
                                .then((user) => {
                                    socket.emit("allMessages", { chatObj: res, friend: user });
                                });
                        });
                        return;
                    }
                    User.findById(friendId)
                        .then((user) => {
                            socket.emit("allMessages", { chatObj: chat, friend: user });
                        });
                });
        });
        socket.on("getMessages", (obj) => {
            Chat.findById(obj.chatId)
                .populate({
                    path: "history",
                    match: { _id: { $lt: obj.lastId } },
                    options: {
                        limit: 10,
                        sort: { _id: -1 }
                    },
                    populate: {
                        path: "sender",
                        select: "firstName lastName"
                    }
                })
                .then((chat) => {
                    if (chat.participants[0].toString() === socket.request.user.id.toString()) {
                        socket.emit("oldMessages", { chat: chat, friendId: chat.participants[1] });
                    } else {
                        socket.emit("oldMessages", { chat: chat, friendId: chat.participants[0] });
                    }
                });
        });
        socket.on("chatMessage", function (data) {
            Chat.findById(data.chatid)
                .then((chat) => {
                    var message = new Message({
                        sender: socket.request.user.id,
                        message: data.msg
                    })
                        .save()
                        .then((msg) => {
                            chat.history.push(msg._id);
                            chat.save();
                            var friendId = chat.participants[0].toString() === socket.request.user.id.toString() ? chat.participants[1] : chat.participants[0];
                            var friendSocket = passportSocketIo.filterSocketsByUser(io, function (element) {
                                return friendId.toString() === element.id.toString();
                            })[0];
                            User.findById(friendId)
                                .then(friend => {
                                    friend.friends.forEach(user => {
                                        if (user.friend.toString() === socket.request.user.id.toString()) {
                                            user.unreadMessage = true;
                                            friend.save();
                                        }
                                    });
                                    findSocket(friendSocket.id).emit("chatMessage", { chatid: chat._id, msg: msg, sender: socket.request.user, friendId: socket.request.user.id });
                                    socket.emit("chatMessage", { chatid: chat._id, msg: msg, sender: socket.request.user, friendId: friendId });
                                });
                        });
                });
        });
        socket.on("getfriends", function () {
            User.findById(socket.request.user.id)
                .populate("friends.friend", {
                    _id: 1,
                    firstName: 1,
                    lastName: 1
                })
                .then((user) => {
                    var connectedFriends = [];
                    passportSocketIo.filterSocketsByUser(io, function (element) {
                        var ret = false;
                        user.friends.forEach(function (friend) {
                            ret = element.id.toString() === friend.friend._id.toString();
                        });
                        return ret;
                    }).forEach(function (followerSocket) {
                        connectedFriends.push(followerSocket.request.user.id);
                        findSocket(followerSocket.id).emit("userConnected", { user: socket.request.user });
                    });
                    socket.emit("friends", { friends: user.friends, connected: connectedFriends });
                });
        });
        socket.on("msgRead", function (friendId) {
            User.findById(socket.request.user.id)
                .then((user) => {
                    user.friends.forEach(friend => {
                        if (friend.friend.toString() === friendId.toString()) {
                            friend.unreadMessage = false;
                            user.save();
                        }
                    });
                });
        });
        socket.on("groupMessage", function (data) {
            User.findById(socket.request.user.id)
                .then(user => {
                    user.friends.forEach(relObj => {
                        Chat.findOne({
                            $or: [
                                { participants: [socket.request.user.id, relObj.friend] },
                                { participants: [relObj.friend, socket.request.user.id] }
                            ]
                        })
                            .then((chat) => {
                                var message = new Message({
                                    sender: socket.request.user.id,
                                    message: data.msg
                                }).save()
                                    .then(msg => {
                                        if (!chat) {
                                            var newChat = new Chat({
                                                participants: [socket.request.user.id, relObj.friend],
                                                history: [msg._id]
                                            }).save();
                                        } else {
                                            chat.history.push(msg._id);
                                            chat.save();
                                        }
                                        var friendId = chat.participants[0].toString() === socket.request.user.id.toString() ? chat.participants[1] : chat.participants[0];
                                        var friendSocket = passportSocketIo.filterSocketsByUser(io, function (element) {
                                            return friendId.toString() === element.id.toString();
                                        })[0];
                                        User.findById(friendId)
                                            .then(friend => {
                                                friend.friends.forEach(user => {
                                                    if (user.friend.toString() === socket.request.user.id.toString()) {
                                                        user.unreadMessage = true;
                                                        friend.save();
                                                    }
                                                });
                                                findSocket(friendSocket.id).emit("chatMessage", { chatid: chat._id, msg: msg, sender: socket.request.user, friendId: socket.request.user.id });
                                            });
                                    });
                            });
                    });
                });
        });
    });

    return router;
};
