module.exports = {
    login: (req, res) => {
        res.render("login");
    },

    authorize: (req, res, next) => {
        if (req.isAuthenticated()) {
            next();
        } else {
            res.redirect("/login");
        }
    },

    logout: (req, res) => {
        req.logout();
        res.redirect("/");
    }
};
