module.exports = (authorize, socketio, passportSocketIo) => {
    var router = require("express").Router();
    var mongoose = require("mongoose");
    var User = mongoose.model("User");
    var Post = mongoose.model("Post");
    var Chat = mongoose.model("Chat");
    var Message = mongoose.model("Message");
    var views = require("../views/main/loader");
    var path = require("path");
    var fs = require('fs');
    var file = fs.readFileSync(path.join(__dirname, "../views/post/index.ejs"), "utf-8");
    var postTemplate = require("ejs").compile(file);

    router.get("/index", authorize, (req, res) => {
        res.render(views.index, { user: req.user, loginUser: req.user });
    });

    router.get("/user/:userid", authorize, (req, res) => {
        User.findById(req.params.userid)
            .then((user) => {
                if (user.isFriend(req.user.id)) {
                    User.isFollowing(req.user.id, req.params.userid, isFollowing => {
                        res.render(views.index, { user: user, loginUser: req.user, isFollowing: isFollowing });
                    });
                } else {
                    res.redirect("/");
                }
            });
    });

    router.get("/loadinvitations", (req, res) => {
        User.findById(req.user.id)
            .populate("invitations", {
                _id: 1,
                firstName: 1,
                lastName: 1
            })
            .then(function (user) {
                res.json({ invitations: user.invitations });
            });
    });

    var main = socketio.on("connection", (socket) => {
        socket.on("sendInvitation", function (userId) {
            User.findById(userId)
                .then(function (user) {
                    user.invitations.push(socket.request.user.id);
                    var inviterUser = passportSocketIo.filterSocketsByUser(socketio, function (element) {
                        return element.id.toString() === userId.toString();
                    })[0];
                    // socketio.sockets.emit("msg", "l")
                    inviterUser.emit("invite", socket.request.user);
                    socket.emit("msg", "q");

                    user.save();
                });
        });
        socket.on("startFollowing", function (userId) {
            User.findById(socket.request.user.id)
                .then((user) => {
                    user.startFollowing(userId);
                });
        });
        socket.on("stopFollowing", function (userId) {
            User.findById(socket.request.user.id)
                .then((user) => {
                    user.stopFollowing(userId);
                });
        });
        socket.on("confirmInvitation", function (userId) {
            User.findById(socket.request.user.id)
                .then((user) => {
                    user.friends.push({ friend: userId, isFollowing: true });
                    user.invitations.pull(userId);
                    user.save();

                    User.findById(userId)
                        .then((user) => {
                            user.friends.push({ friend: socket.request.user.id, isFollowing: true });
                            user.invitations.pull(socket.request.user.id);
                            user.save();
                        });
                });
        });
        socket.on("toggleLike", function (postId) {
            Post.findById(postId)
                .then(function (post) {
                    var added = post.toggleLike(socket.request.user.id);
                    User.getFollowers(post.userWall, (followers) => {
                        passportSocketIo.filterSocketsByUser(socketio, function (element) {
                            var ret = false;
                            followers.forEach(function (follower) {
                                if (element.id.toString() === follower._id.toString() ||
                                    element.id.toString() === post.creator.toString() ||
                                    element.id.toString() === post.userWall.toString()) {
                                    ret = true;
                                }
                            });
                            return ret;
                        }).forEach(function (followerSocket) {
                            followerSocket.emit("postLike", { user: socket.request.user, id: post._id, added: added });
                        });
                    });
                });
        });
        socket.on("toggleShare", function (postId) {
            Post.findById(postId)
                .then(function (post) {
                    var added = post.toggleShare(socket.request.user.id);
                    User.getFollowers(post.userWall, (followers) => {
                        passportSocketIo.filterSocketsByUser(socketio, function (element) {
                            var ret = false;
                            followers.forEach(function (follower) {
                                if (element.id.toString() === follower._id.toString() ||
                                    element.id.toString() === post.creator.toString() ||
                                    element.id.toString() === post.userWall.toString()) {
                                    ret = true;
                                }
                            });
                            return ret;
                        }).forEach(function (followerSocket) {
                            followerSocket.emit("postShare", { user: socket.request.user, id: post._id, added: added });
                        });
                    });
                });
        });
        socket.on("error", function (err) {
            // console.log(err);
        });
        socket.on("getfriends", function () {
            var obj;
            User.findById(socket.request.user.id)
                .populate("friends.friend", {
                    _id: 1,
                    firstName: 1,
                    lastName: 1
                })
                .then((user) => {
                    var connectedFriends = [];
                    passportSocketIo.filterSocketsByUser(socketio, function (element) {
                        var ret = false;
                        user.friends.forEach(function (friend) {
                            if (element.id.toString() === friend.friend._id.toString()) {
                                ret = true;
                            }
                        });
                        return ret;
                    }).forEach(function (followerSocket) {
                        connectedFriends.push(followerSocket.request.user.id);
                        followerSocket.emit("userConnected", { user: socket.request.user });
                    });
                    socket.emit("friends", { friends: user.friends, connected: connectedFriends });
                });
        });
        socket.on("disconnect", function () {
            var obj;
            User.findById(socket.request.user.id)
                .then((user) => {
                    passportSocketIo.filterSocketsByUser(socketio, function (element) {
                        var ret = false;
                        user.friends.forEach(function (friend) {
                            if (element.id.toString() === friend.friend.toString()) {
                                ret = true;
                            }
                        });
                        return ret;
                    }).forEach(function (followerSocket) {
                        followerSocket.emit("friendDisconnected", { user: socket.request.user });
                    });
                });
        });
        socket.on("openChat", function (friendId) {
            Chat.findOne({ $or: [{ participants: [socket.request.user.id, friendId] }, { participants: [friendId, socket.request.user.id] }] })
                .populate("history", {
                    message: 1
                })
                .then((chat) => {
                    console.log(chat);
                    if (!chat) {
                        var newChat = new Chat({
                            participants: [socket.request.user.id, friendId],
                            history: []
                        }).save((err, res) => {
                            User.findById(friendId)
                                .then((user) => {
                                    socket.emit("allMessages", { chatObj: res, friend: user });
                                });
                        });
                        return;
                    }
                    User.findById(friendId)
                        .then((user) => {
                            socket.emit("allMessages", { chatObj: chat, friend: user });
                        });
                });
        });
        socket.on("chatMessage", function (data) {
            Chat.findById(data.chatid)
                .then((chat) => {
                    var message = new Message({
                        sender: socket.request.user.id,
                        message: data.msg
                    }).save()
                        .then((msg) => {
                            chat.history.push(msg._id);
                            chat.save();
                            passportSocketIo.filterSocketsByUser(socketio, function (element) {
                                if (element.id.toString() === chat.participants[0].toString() ||
                                    element.id.toString() === chat.participants[1].toString()) {
                                    return true;
                                }
                            }).forEach(function (followerSocket) {
                                followerSocket.emit("chatMessage", { chatid: chat._id, msg: msg, sender: socket.request.user.id });
                            });
                        });
                });
        });

        User.findById(socket.request.user.id)
            .then((user) => {
                passportSocketIo.filterSocketsByUser(socketio, function (element) {
                    var ret = false;
                    user.friends.forEach(function (friend) {
                        if (element.id.toString() === friend.friend.toString()) {
                            ret = true;
                        }
                    });
                    return ret;
                }).forEach(function (followerSocket) {
                    followerSocket.emit("friendConnected", { user: socket.request.user });
                });
            });
    });

    return router;
};
