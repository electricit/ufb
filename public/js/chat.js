$(function () {
    var messageLoading = false;
    var readCookie = function (cookieName) {
        var name = cookieName + "=";
        var cookieArray = document.cookie.split(';');
        for (let i = 0; i < cookieArray.length; i++) {
            let cookie = cookieArray[i];
            while (cookie.charAt(0) == ' ') {
                cookie = cookie.substring(1);
            }
            if (cookie.indexOf(name) === 0) {
                return cookie.substring(name.length, cookie.length);
            }
        }
        return "";
    };

    var createMessageWindow = function (data) {
        var container = $("<div class=\"message\" data-chatid=\"" + data.chatObj._id + "\">");
        var header = $("<div class=\"header\">").text(data.friend.fullName);
        var buttonX = $("<button data-friendid=\"" + data.friend.id + "\">").text("X");
        var content = $("<div class=\"content\">");
        var ul = $("<ul>");
        var footer = $("<div class=\"footer\">");
        var input = $("<input type=\"text\" id=\"input-message\"/>");
        var buttonSend = $("<button>").text(">");

        data.chatObj.history.forEach(function (el) {
            addMessage(ul, el, data.friend.id);
        });
        container.addClass(data.chatObj._id);
        container
            .append(header.append(buttonX))
            .append(content
                .append(ul))
            .append(footer
                .append(input)
                .append(buttonSend));
        $(".messages").prepend(container);
        ul.parent(".content").animate({ scrollTop: ul.prop("scrollHeight") }, 0);
        /*<div class="messages">
            <div class="header">
                <button>X</button>
            </div>
            <div class="content">wiadomosci</div>
            <div class="footer">
                <input type="text" id="input-message"/>
                <button>></button>
            </div>
        </div>*/
    };

    var addMessage = function (chatUl, obj, friendId, prepend) {
        var li = $("<li>");
        var divHead = $("<div>");
        var name = $("<span>");
        var divMessage = $("<div>");

        divHead.addClass("head");
        name.addClass("name");
        divMessage.addClass("msg");
        divHead.append(name);
        name.text(obj.sender.firstName + " " + obj.sender.lastName);
        divMessage.text(obj.message);
        if (obj.sender.id !== friendId) {
            li.addClass("own");
        }
        li.append(divHead);
        li.append(divMessage);
        if (obj.id) {
            li.data("msgId", obj.id);
        } else {
            li.data("msgId", obj._id);
        }
        if (!prepend) {
            chatUl.append(li);
        } else {
            chatUl.prepend(li);
        }
    };

    var socket = io.connect("/chat", {
        query: "session_id=" + readCookie("express.sid")
    });

    socket.on("allMessages", function (data) {
        createMessageWindow(data);
    });

    socket.on("chatMessage", function (data) {
        var chat = $(".messages ." + data.chatid);
        if (chat.length !== 0) {
            var chatUl = chat.find("ul");
            addMessage(chatUl, {
                message: data.msg.message,
                sender: data.sender
            }, data.friendId);

            chatUl.parent(".content").animate({ scrollTop: chatUl.prop("scrollHeight") }, 700);
            socket.emit("msgRead", data.friendId);
        } else {
            $(".friends ul ." + data.sender.id).addClass("read-message");
        }
    });

    socket.on("oldMessages", function (data) {
        if (data.chat.history === 0) return;
        var chatUl = $(".messages ." + data.chat._id).find("ul");
        var oldHeight = chatUl.parent().prop("scrollHeight");
        var oldScroll = chatUl.parent().scrollTop();
        data.chat.history.forEach(function (message) {
            addMessage(chatUl, message, data.friendId, true);
        });
        chatUl.parent().scrollTop(chatUl.parent().prop("scrollHeight") - oldHeight);
        messageLoading = false;
    });

    $(".friends ul").on("click", "li", function () {
        var id = $(this).data("userid");
        if ($(this).hasClass("open")) {
            return;
        }

        $(this).removeClass("read-message");
        $(this).addClass("open");
        socket.emit("openChat", id);
        socket.emit("msgRead", id);
    });

    $(".messages").on("click", ".header button", function () {
        $(this).parents(".message").remove();
        var friendId = $(this).data("friendid");
        $(".friends li." + friendId).removeClass("open");
    });

    $(".messages").on("click", ".footer button", function () {
        var input = $(".messages .footer input");
        var msg = input.val();
        input.val("");
        socket.emit("chatMessage", { chatid: $(this).parents(".message").data("chatid"), msg: msg });
    });

    var messages = document.getElementsByClassName("messages")[0];
    messages.addEventListener('scroll', function (event) {
        if (event.target.classList.contains("content")) {
            if (messageLoading)
                return;
            if (event.target.scrollTop === 0) {
                messageLoading = true;
                var chatid = $(event.target).parent().data("chatid");
                var lastMessageId = $(event.target).find("ul > li:first-child").data("msgId");

                socket.emit("getMessages", { chatId: chatid, lastId: lastMessageId });
            }
        }
    }, true);

    setTimeout(function () {
        socket.emit("getfriends");
    }, 300);

    socket.on("friends", function (data) {
        $(".friends ul li").remove();
        data.friends.forEach(function (element) {
            var li = $("<li data-userid=\"" + element.friend._id + "\"class=\"" + element.friend._id + "\">").text(element.friend.fullName);
            if (data.connected.indexOf(element.friend._id) > -1) {
                li.addClass("connected");
            }

            if (element.unreadMessage) {
                li.addClass("read-message");
            }
            $(".friends ul").append(li);
        });
    });

    $("#send-group-message").click(function() {
        var grpMsg = $("#group-message");
        var msg = grpMsg.val();
        grpMsg.val("");
        socket.emit("groupMessage", {msg: msg});
    });
});
