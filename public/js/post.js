$(function () {
    var postsToLoad = [];
    var readCookie = function (cookieName) {
        var name = cookieName + "=";
        var cookieArray = document.cookie.split(';');
        for (let i = 0; i < cookieArray.length; i++) {
            let cookie = cookieArray[i];
            while (cookie.charAt(0) == ' ') {
                cookie = cookie.substring(1);
            }
            if (cookie.indexOf(name) === 0) {
                return cookie.substring(name.length, cookie.length);
            }
        }
        return "";
    };

    var drawPost = function (post, pre) {
        var container = $(post);
        if (pre) {
            $(".post-container").prepend(container);
        } else {
            $(".post-container").append(container);
        }
    };

    var socket = io.connect("/post", {
        query: "session_id=" + readCookie("express.sid")
    });

    $("#publish").click(function (e) {
        e.preventDefault();
        var content = $("#create-post").val();
        $("#create-post").val("");

        var wallId = $("#wall").data("wallid");

        var json = {
            content: content,
            userwall: wallId
        };
        socket.emit("addPost", json);
    });

    socket.on("newPost", function (postObj) {
        var wall = $("#wall");
        var wallId = wall.data("wallid");
        var loginUser = wall.data("loginuser");
        if (wallId === loginUser)
            return;
        if ($("#" + postObj.post._id).length !== 0)
            return;
        if (wallId !== postObj.post.userWall.id)
            return;
        drawPost(postObj.renderedPost, true);
    });

    socket.on("followerPost", function (postObj) {
        var wall = $("#wall");
        var wallId = wall.data("wallid");
        var loginUser = wall.data("loginuser");
        if (wallId !== loginUser)
            return;
        if ($("#" + postObj.post._id).length !== 0)
            return;
        drawPost(postObj.renderedPost, true);
    });

    socket.on("loadOwnPost", function (post) {
        drawPost(post, true);
    });

    socket.on("nextPosts", function(data) {
        data.posts.forEach(function(post) {
            drawPost(post, false);
        });
    });

    $.ajax({
        url: "/post/get",
        method: "get",
        data: { "wallid": $("#wall").data("wallid") },
        success: function (data) {
            data.posts.forEach(function (element) {
                drawPost(element, true);
            });
        }
    });

    var posts = document.getElementsByClassName("posts")[0];
    window.addEventListener('scroll', function (event) {
            if ($(window).scrollTop() + $(window).height() === $(document).height()) {
                var wallId = $("#wall").data("wallid");
                var lastPostId = $(".post-container .post:last-child").attr("id");
                socket.emit("loadPosts", {wallid: wallId, lastId: lastPostId});
            }
    }, true);
});
