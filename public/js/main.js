$(function () {
    $("#search").on("input", function (e) {
        var val = $("#search").val();
        var list = $("<ul>");

        $.ajax({
            url: "/user/all",
            method: "get",
            data: { "regex": val },
            success: function (data) {
                $(".typeAhead ul").remove();
                data.forEach(function (el) {
                    var li = drawLiElement(el);
                    list.prepend(li);
                });
                $(".typeAhead").append(list);
            }
        });

        var drawLiElement = function (user) {
            var li = $("<li id=\"" + user._id + "\">")
                .append($("<a href=\"/main/user/" + user._id + "\">")
                    .text((user.firstName + " " + user.lastName)));
            var userLink = $("<div class=\"user-link\">");
            li.append(userLink);
            if (user.isFriend) {
                li.addClass("friend");
            } else {
                if (user._id !== $("#wall").data("loginuser")) {
                    var invite = $("<button class=\"invite\" id=\"" + user._id + "\">");
                    invite.text("Zaproś");
                    userLink.append(invite);
                }
            }
            return li;
        };
    });

    $("#search").focusout(function () {
        $(this).val("");

        setTimeout(function () {
            $(".typeAhead ul").remove();
        }, 200);
    });

    var readCookie = function (cookieName) {
        var name = cookieName + "=";
        var cookieArray = document.cookie.split(';');
        for (let i = 0; i < cookieArray.length; i++) {
            let cookie = cookieArray[i];
            while (cookie.charAt(0) == ' ') {
                cookie = cookie.substring(1);
            }
            if (cookie.indexOf(name) === 0) {
                return cookie.substring(name.length, cookie.length);
            }
        }
        return "";
    };

    var socket = io("/", {
        query: "session_id=" + readCookie("express.sid")
    });

    socket.on("msg", function (msg) {
        //console.log(msg);
    });

    socket.on("invite", function (user) {
        drawInvitations(user);
    });

    var drawInvitations = function (user) {
        var li = $("<li data-id=\"" + user.id + "\">")
            .text(user.firstName + " " + user.lastName)
            .append($("<button class=\"confirm\">")
                .text("Potwierdź"));
        $(".invitations ul").append(li);
        var button = $("#show-invitations");
        button.addClass("new-invitations");
    };

    $.ajax({
        url: "/main/loadinvitations",
        method: "get",
        success: function (data) {
            data.invitations.forEach(function (invitation) {
                drawInvitations(invitation);
            });
        },
        error: function () {
            //console.log("blad pobierania zaproszen")
        }
    });

    socket.on("postLike", function (data) {
        if (data.added) {
            var li = $("<li class=\"" + data.user.id + "\">").text(data.user.firstName + " " + data.user.lastName);
            $("#" + data.id).find(".likes").append(li);
        } else {
            $("#" + data.id).find(".likes ." + data.user.id).remove();
        }
    });

    socket.on("postShare", function (data) {
        if (data.added) {
            var li = $("<li class=\"" + data.user.id + "\">").text(data.user.firstName + " " + data.user.lastName);
            $("#" + data.id).find(".shares").append(li);
        } else {
            $("#" + data.id).find(".shares ." + data.user.id).remove();
        }
    });

    socket.on("friendDisconnected", function (data) {
        $(".friends ul ." + data.user.id).removeClass("connected");
    });

    socket.on("friendConnected", function (data) {
        $(".friends ul ." + data.user.id).addClass("connected");
    });

    $(".actions .follow").click(function() {
        var wallid = $("#wall").data("wallid");
        if ($(this).hasClass("following")) {
            socket.emit("stopFollowing", wallid);
            $(this).text("Obserwuj");
        } else {
            socket.emit("startFollowing", wallid);
            $(this).text("Nie obserwuj");
        }
    });

    $(".typeAhead").on("click", "ul li .invite", function () {
        var id = $(this).parents("li").attr("id");
        socket.emit("sendInvitation", id);
    });

    $("#show-invitations").click(function () {
        $(".invitations ul").slideToggle();
    });

    $(".invitations ul").on("click", ".confirm", function (data) {
        var id = $(this).parents("li").data("id");
        $("#show-invitations").trigger("click");
        $(this).parents("li").remove();
        socket.emit("confirmInvitation", id);
        var button = $("#show-invitations");
        socket.emit("getfriends");
        if ($(".invitations ul li").length !== 0)
            return;
        button.removeClass("new-invitations");
    });

    $(".post-container").on("click", ".like", function () {
        var id = $(this).parents(".post").attr("id");
        socket.emit("toggleLike", id);
    });

    $(".post-container").on("click", ".share", function () {
        var id = $(this).parents(".post").attr("id");
        socket.emit("toggleShare", id);
    });

    $(".show-chat").click(function() {
        $(".friends").slideToggle();
    });
});
