$(function () {
    $("#signin").submit(function (e) {
        e.preventDefault();
        var handler = function (data) {
            if (data.error) {
                showAlert(data.error);
                return;
            }
            document.location.replace("/");
        }
        sendRequest(this, "/login", handler);
    });

    $("#signup").submit(function (e) {
        e.preventDefault();
        var handler = function (data) {
            if (data.error) {
                showAlert(data.error);
                return;
            }
            document.location.replace("/");
        };


        sendRequest(this, "/user/signup", handler);
    });

    $("#confirm-password").focusout(function () {
        var pass = document.getElementById("password");
        var confPass = document.getElementById("confirm-password");
        if (pass.value !== confPass.value) {
            confPass.setCustomValidity("Hasła muszą być takie same");
            $(confPass).css({
                "border-color": "red"
            });
            $(pass).css({
                "border-color": "red"
            });
            return;
        } else {
            confPass.setCustomValidity("");
            $(confPass).css({
                "border-color": "#fff"
            });
            $(pass).css({
                "border-color": "#fff"
            });
        }
    });

    var getFormJson = function (form) {
        var unindexedArray = form.serializeArray();
        var indexedArray = {};

        $.map(unindexedArray, function (n, i) {
            indexedArray[n.name] = n.value;
        });

        return indexedArray;
    }

    var showAlert = function (error) {
        var alert = $(".alert");
        alert.css("opacity", 1);
        alert.css("visibility", "visible");
        alert.html(error);
        setTimeout(function () {
            alert.css("opacity", 0);
            setTimeout(function () {
                alert.css("visibility", "hidden");
            }, 1000);
        }, 5000);
    };

    var sendRequest = function (obj, relUrl, handler) {
        var data = $(obj).serializeArray();
        var json = getFormJson($(obj));

        $.ajax({
            url: relUrl,
            method: "post",
            contentType: "application/json",
            data: JSON.stringify(json),
            success: handler,
            error: function (xhr, status) {
                // showAlert()
            }
        });
    };
});
