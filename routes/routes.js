var express = require("express");
var authController = require("../controllers/authController");
var userController = require("../controllers/userController");
var path = require("path");

module.exports = (passport, socketio, passportSocketIo) => {
    var router = express.Router();
    var mainController = require("../controllers/mainController");

    //account
    router.route("/login")
        .get(authController.login)
        .post((req, res, next) => {
            passport.authenticate('local', function (err, user, info) {
                if (err)
                {
                    return next(err);
                }
                if (!user)
                {
                    return res.json({error: info.message});
                }
                req.login(user, function (err) {
                    if (err)
                    {
                        return next(err);
                    }
                    return res.json(user);
                });
            })(req, res, next);
        });
    router.get("/logout", authController.logout);
    router.get("/", (req, res) => {
        res.redirect("/main/index");
    });

    var fs = require("fs");
    fs.readdirSync(path.join(__dirname, "../controllers")).forEach((file) => {
        if (file === "authController.js") {
            return;
        }
        var indexToSubstring = file.indexOf("Controller");
        var controllerName = file.substring(0, indexToSubstring);
        var controller = require(path.join(__dirname, "../controllers", file))(authController.authorize, socketio, passportSocketIo);
        router.use("/" + controllerName, controller);
    });

    return router;
};
